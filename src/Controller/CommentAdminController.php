<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CommentRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN_COMMENT")
 */
class CommentAdminController extends AbstractController
{
    /**
     * @Route("/admin/comment", name="comment_admin")
     */
    public function index(CommentRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        //Per controlar l'accés:
        /*$this->denyAccessUnlessGranted('ROLE_ADMIN');*/


        //Així ens mostrarà els últims comments a dalt
        $comments = $repository->findBy([], ['createdAt' => 'DESC']);

        $q = $request->query->get('q');
        //dump($q); exit();

        //Cridem a la funció que hem fet al CommentRepo per buscar els resultats
        $queryBuilder = $repository->getWithSearchQueryBuilder($q);

        //dump($comments); exit();

        //Paginator
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('comment_admin/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
