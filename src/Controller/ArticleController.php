<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 30/01/19
 * Time: 18:40
 */

namespace App\Controller;

use App\Repository\CommentRepository;
use App\Service\SlackClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;
use App\Repository\ArticleRepository;


class ArticleController extends AbstractController
{
    /**
     * @var
     */
    private $isDebug;
    /**
     * @var SlackClient
     */
    private $slack;

    public function __construct(bool $isDebug, SlackClient $slack)
    {
        //dump($isDebug);die;
        $this->isDebug = $isDebug;
        $this->slack = $slack;
    }
    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage(ArticleRepository $repository){

        //$repository = $em->getRepository(Article::class);
        $articles = $repository->findAllPublishedOrderedByNewest();

        //dump($repository); exit();
        return $this->render('article/homepage.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/news/{slug}", name="article_show")
     */
    public function show(Article $article,  SlackClient $slack)
    {
        if ($article->getSlug() === 'khaaaaaan') {
            $slack->sendMessage('Kahn', 'Ah, Kirk, my old friend...');
        }




        //dump($comments); exit();
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     */
    public function toggleArticleHeart(Article $article, LoggerInterface $logger, EntityManagerInterface $em){

        $article->incrementHeartCount();
        $em->flush();
        $logger->info('Article is being hearted!');
        return new JsonResponse(['hearts' => $article->getHeartCount()]);

    }
}