<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Tag;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\User;
use App\DataFixtures\UserFixtures;

class ArticleFixtures extends BaseFixtures
{
    private static $articleTitles = [
        'Migrations: Creating the Database Tables/Schema?',
        'Overriding the Default Error Templates',
        'Light Speed Travel: Fountain of Youth or Fallacy',
    ];
    private static $articleImages = [
        'lightspeed.png',
        'mercury.jpeg',
        'asteroid.jpeg'
    ];


    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Article::class, 10, function(Article $article, $count) use ($manager) {
            $article->setTitle($this->faker->randomElement(self::$articleTitles))
                /*->setSlug($this->faker->slug)*/
                ->setContent(<<<EOF
Earlier research had shown that isotopes of some radioactive elements decay into other elements at a predictable rate. By examining the existing elements, scientists can calculate the initial quantity of a radioactive element, and thus how long it took for the elements to decay, allowing them to determine the age of the rock.
The oldest rocks on Earth found to date are the Acasta Gneiss in northwestern Canada near the Great Slave Lake, which are 4.03 billion years old. But rocks older than 3.5 billion years can be found on all continents. Greenland boasts the Isua supracrustal rocks (3.7 to 3.8 billion years old), while rocks in Swaziland are 3.4 billion to 3.5 billion years. 
Samples in Western Australia run 3.4 billion to 3.6 billion years old.                               
EOF
                );
            // fem servir el Faker per generar el percentatge d'articles publicats, un 70%en aq cas
            // i tb per setejar una data
            // publish most articles
            if ($this->faker->boolean(90)) {
                $article->setPublishedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            }
            //Add nom de l'autor
            $article->setAuthor($this->getRandomReference('main_users'))
                ->setHeartCount($this->faker->numberBetween(1, 5))
                ->setImageFilename($this->faker->randomElement(self::$articleImages));

            //Carreguem de 0 a 5 tags per a cada article
            $tags = $this->getRandomReferences(Tag::class, $this->faker->numberBetween(0, 5));

            foreach ($tags as $tag) {
                $article->addTag($tag);
                /*$tag->getName();
                dump($tag);*/
            }


        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            TagFixtures::class,
            UserFixtures::class,
        ];
    }
}
