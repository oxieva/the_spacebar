<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Article;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixture extends BaseFixtures implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 50, function(Comment $comment) {
            $comment->setContent(
                $this->faker->boolean ? $this->faker->paragraph : $this->faker->sentences(2, true)
            );
            $comment->setAuthorName($this->faker->name);
            $comment->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'));
            //Dels 50 comments add, en marcarem 20 com a borrats

            $comment->setIsDeleted($this->faker->boolean(20));
            /*Aquí estem creant 50 comments al pròxim article que crearem carregant les fixtures
            Jo faig q en crei 2 de nous cada cop, doncs al primer d'aqs dos, el article amb id 37, tindrem 50 comments*/
            //Ara fem q en comptes d'afegirlos al 1er article, els afegim entre el 0 i el9
            /*$comment->setArticle($this->getReference(Article::class.'_'. $this->faker->numberBetween(0, 9)));*/
            //Cridem la funció cridada per en Ryan a BaseFixtures pq sigui tot aleatori
            $comment->setArticle($this->getRandomReference(Article::class));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [ArticleFixtures::class];
    }
}
