-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 21-02-2019 a les 13:20:01
-- Versió del servidor: 5.7.25-0ubuntu0.16.04.2
-- Versió de PHP: 7.2.10-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `the_spacebar`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `api_token`
--

CREATE TABLE `api_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `api_token`
--

INSERT INTO `api_token` (`id`, `user_id`, `token`, `expires_at`) VALUES
(1, 1, 'cc274eb54d1864f80b6afc98057d08145d63d6857937209137d7651e4aaf9430b66734ae7b2d8eef4aebeb21a8fbd5e087d4486a7a735259f5f5f888', '2019-02-21 13:13:08'),
(2, 1, '27d376c9897bb26f7696532c84e5414bf977cbc7ece4c84edb29d5513528460028e782d6f1ea437ff9defa585e37318af785f7af69999e8878118b60', '2019-02-21 13:13:08'),
(3, 2, 'b0ee620d5c37b3bf8a2a4cd70ed2aefafabdfd4fc976a281e410684290a8e197599a4c2d6631e6e6217a317df475cb99075e8d6aba124300bf323b07', '2019-02-21 13:13:09'),
(4, 2, '244dc86bd436a97db93038125de0782850749881c174885ef894402d9a0cbb8cde672f32e807a26389023fbc67dfd44b2edb53a2c14f4745731e0f44', '2019-02-21 13:13:09'),
(5, 3, '3bdb83a2bc30c48291754377d1a7598fd5d6d1fd4a5130aa78ab938466a1a58d12a90f07e38857a26be5a1bb426a8b3c842abedcad03a78fe1d95e7a', '2019-02-21 13:13:09'),
(6, 3, 'a8da3de45c6f58306cb05c379e8bb431d1ab3b6c6866681bf87d2b26200d5a1dd1c69c0292a2888ec1019b074ba3b39aa15aa2415dbdb78a98402726', '2019-02-21 13:13:09'),
(7, 4, '7a9afad0cf8522ddef262fe19da5eacc69c6a544917f95f40ef478dde3a188f01c7e8628017b5765862759c8333022ee69a62235554b90704d011796', '2019-02-21 13:13:10'),
(8, 4, '9f33b13a39e1ad882128d066337ad715391cbdac2f93b76e9437c9cbca6787933f58aa824f242454b849f06e06de5503fb351acb52287354dde08d8c', '2019-02-21 13:13:10'),
(9, 5, '652109cdc6977bb38998d0c8f5d36bef61f5e5031f09bda4b7995ba0fabf9c87af254c0414903ac20ea9e7c6082644fce6df50adf5ca251fcd244e4d', '2019-02-21 13:13:11'),
(10, 5, '49c11d12ac08bef8db5e699b91e126359073edb5cdb2cedba9a706a869c0d4f2add866f2fced886ce37d7d328b8af1b5adbc258e1be3b8d0a03fa083', '2019-02-21 13:13:11'),
(11, 6, 'c7c30c1944f8bd51e7c57172c8792421718091361924f3d26d0a09253fa1a4ed3f57467921daca65bc8ca0572b783988b958f40b7c9adb47011345d1', '2019-02-21 13:13:11'),
(12, 6, '16ea9a9b2e8ee2ff79b4528f804fbd0f10035ab40a6d4fd2a771a4cdd809340057b6962069755db82660b5a93667701362175d7a30cc81a454eea3fd', '2019-02-21 13:13:11'),
(13, 7, 'b1e0cf4097214c99d0a30d7e2c809cf2b80e57672701b568be74c4be6a584206d951f88e4bbf3e7265428845dea8fcd8dbedbc3ff05be7ce5b321096', '2019-02-21 13:13:12'),
(14, 7, 'ebd1d5b8c206dca116206483bbb89d2d8ba5782877202208a88a7fcf2e82ff240040e32c475a68c85694470c415167605b28b14c5c437a2d87a569ac', '2019-02-21 13:13:12'),
(15, 8, 'bbfd0c797ef23e6f48ebc0e01916f8316ebb3881190c36c86e7080b9d21172d6353fa67206cd73ca5237d3579b0b44702f8c6acc0724f4d477b195e4', '2019-02-21 13:13:12'),
(16, 8, 'd20f526ef53199acb0c27a495af1032845d776fe35885e5cfef978e24579d0c093eca5180422a929dab07bae3cfb5bb2264a762bd8fe0befdb1067b2', '2019-02-21 13:13:12'),
(17, 9, '7352f01e795ca73300891a0e9b92a72d5845a87a0f50dd4c35572a74067f1055f893ef73e327b22b5d7bec983f6896b6b07228d28dbcaf7f767f389c', '2019-02-21 13:13:13'),
(18, 9, 'd7605604f8cdc175f3ff4e1c131d43391c90d2824e8425651c12da2732df62b771322f6e2f6f1b2bdf8f116f552ce693e30ac23fe54daeadc2c0dc5d', '2019-02-21 13:13:13'),
(19, 10, 'a7d6239f4556bc517d1190941cd795dea8b97d41d362dc03b83d33b00c625a5ce7edf283cc02f2d5a6295acb4f4d1c9643aef34dff9d98a300eada85', '2019-02-21 13:13:13'),
(20, 10, '44b4664df94d28ee94d36e780d3537e1eeed53e63d19efe2777cf3ad65cb199b9ad05aee79f562b850e77bb4a62ec9f814efe992e1d452abab6c5069', '2019-02-21 13:13:13');

-- --------------------------------------------------------

--
-- Estructura de la taula `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `published_at` datetime DEFAULT NULL,
  `heart_count` int(11) NOT NULL,
  `image_filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `article`
--

INSERT INTO `article` (`id`, `title`, `slug`, `content`, `published_at`, `heart_count`, `image_filename`, `created_at`, `updated_at`, `author_id`) VALUES
(1, 'How old are your rocks?', 'how-old-are-your-rocks', 'Scientists have made several attempts to date the planet over the past 400 years. They\\\'ve attempted to predict the age based on changing sea levels, the time it took for Earth or the sun to cool to present temperatures, and the salinity of the ocean. As the dating technology progressed, these methods proved unreliable; for instance, the rise and fall of the ocean was shown to be an \\never-changing process rather than a gradually declining one.    ', '2019-02-03 00:00:00', 1, 'lightspeed.png', '2019-02-03 00:00:00', '2019-02-04 00:00:00', 1),
(2, 'Life on Planet Mercury: Tan, Relaxing and Fabulous', 'life-on-planet-mercury-tan-relaxing-and-fabulous', 'Earlier research had shown that isotopes of some radioactive elements decay into other elements at a predictable rate. By examining the existing elements, scientists can calculate the initial quantity of a radioactive element, and thus how long it took for the elements to decay, allowing them to determine the age of the rock.\\nThe oldest rocks on Earth found to date are the Acasta Gneiss in northwestern Canada near the Great Slave Lake, which are 4.03 billion years old. But rocks older than 3.5 billion years can be found on all continents. Greenland boasts the Isua supracrustal rocks (3.7 to 3.8 billion years old), while rocks in Swaziland are 3.4 billion to 3.5 billion years. \\nSamples in Western Australia run 3.4 billion to 3.6 billion years old.', '2019-02-06 00:00:00', 2, 'asteroid.jpeg', '2019-02-01 00:00:00', '2019-02-02 00:00:00', 4),
(5, 'Life on Planet Mercury: Tan, Relaxing and Fabulous', 'life-on-planet-mercury-tan-relaxing-and-fabulous-1', 'The ESA’s Mars Express orbiter has spotted a funny cloud on Mars, right near the Arsia Mons Volcano. At first glance it looks like a plume coming out of the volcano. But it’s formation is not related to any internal activity in this long-dead volcano. It’s a cloud of water ice known as an orographic or lee cloud.\r\n\r\nThe cloud isn’t linked to any volcanic activity, but its formation is associated with the form and altitude of Arsia Mons. Arsia Mons is a dormant volcano, with scientists putting its last eruptive activity at 10 mya. This isn’t the first time this type of cloud has been seen hovering around Arsia Mons.', '2019-02-15 00:00:00', 2, 'mercury.jpeg', '2019-02-01 00:00:00', '2019-02-06 00:00:00', 9),
(6, 'Why Asteroids Taste Like Bacon', 'why-asteroids-taste-like-bacon', '\r\nPOSTED ONFEBRUARY 13, 2019\r\nThere’s Evidence that Mars is Still Volcanically Active\r\n The South Pole on Mars. Image: NASA.\r\nA new study shows that Mars may very well be volcanically active. Nobody’s seen direct evidence of volcanism; no eruptions or magma or anything like that. Rather, the proof is in the water.\r\n\r\nContinue reading “There’s Evidence that Mars is Still Volcanically Active”\r\nPOSTED ONOCTOBER 27, 2018\r\nThere’s a Funny Cloud on Mars, Perched Right at the Arsia Mons Volcano. Don’t Get Too Excited, Though, it’s not an Eruption\r\n A funny cloud on Mars. The ESA\' Mars Express orbiter captured this image of an elongated cloud forming near the Arsia Mons volcano at the Martian equator. Image: ESA/Mars Express\r\n\r\n \r\nThe ESA’s Mars Express orbiter has spotted a funny cloud on Mars, right near the Arsia Mons Volcano. At first glance it looks like a plume coming out of the volcano. But it’s formation is not related to any internal activity in this long-dead volcano. It’s a cloud of water ice known as an orographic or lee cloud.\r\n\r\nThe cloud isn’t linked to any volcanic activity, but its formation is associated with the form and altitude of Arsia Mons. Arsia Mons is a dormant volcano, with scientists putting its last eruptive activity at 10 mya. This isn’t the first time this type of cloud has been seen hovering around Arsia Mons.\r\n\r\nContinue reading “There’s a Funny Cloud on Mars, Perched Right at the Arsia Mons Volcano. Don’t Get Too Excited, Though, it’s not an Eruption”\r\n\r\nPOSTED ONOCTOBER 2, 2018\r\nA Tiny Motor on Curiosity was one of the First Instruments to Notice the Global Martian Dust Storm\r\n A tiny electric motor on the Curiosity rover was one of the first instruments to notice a global Martian dust storm. The storm completely obscured the surface of Mars. Images from May 28 and July 1. Credit: NASA/JPL-Caltech/MSSS\r\nA tiny electric motor on the Curiosity rover played a role in identifying a global Martian dust storm. The storm completely enveloped the planet between May and July, 2018. It was the biggest storm since 2007.\r\n\r\nContinue reading “A Tiny Motor on Curiosity was one of the First Instruments to Notice the Global Martian Dust Storm”\r\n\r\nPOSTED ONFEBRUARY 5, 2018\r\nGood News For The Search For Life, The Trappist System Might Be Rich In Water\r\n \r\nWhen we finally find life somewhere out there beyond Earth, it’ll be at the end of a long search. Life probably won’t announce its presence to us, we’ll have to follow a long chain of clues to find it. Like scientists keep telling us, at the start of that chain of clues is water.\r\n\r\nThe discovery of the TRAPPIST-1 system last year generated a lot of excitement. 7 planets orbiting the star TRAPPIST-1, only 40 light years from Earth. At the time, astronomers thought at least some of them were Earth-like. But now a new study shows that some of the planets could hold more water than Earth. About 250 times more.\r\n\r\nThis new study focuses on the density of the 7 TRAPPIST-1 planets. Trying to determine that density is a challenging task, and it involved some of the powerhouses in the world of telescopes. The Spitzer Space Telescope, the Kepler Space Telescope, and the SPECULOOS (Search for habitable Planets EClipsing ULtra-cOOl Stars) facility at ESO’s Paranal Observatory were all used in the study.', '2019-02-20 00:00:00', 2, 'asteroid.jpeg', '2019-02-07 00:00:00', '2019-02-12 00:00:00', 6);

-- --------------------------------------------------------

--
-- Estructura de la taula `article_tag`
--

CREATE TABLE `article_tag` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de la taula `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `article_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `comment`
--

INSERT INTO `comment` (`id`, `author_name`, `content`, `created_at`, `updated_at`, `article_id`, `is_deleted`) VALUES
(4, 'Arlett Might', 'I hate bacon, OMG!', '2019-02-12 00:00:00', '2019-02-19 00:00:00', 1, 0),
(5, 'Death Ship', 'Woohoo! I am studying owning and inverse sides!!!', '2019-02-21 00:00:00', '2019-02-13 00:00:00', 1, 0),
(25, 'Arlett Might', 'Woohoo! I am studying owning and inverse sides!!!', '2019-02-12 00:00:00', '2019-02-19 00:00:00', 2, 0),
(26, 'Death Ship', 'Now would this be apple wood smoked bacon? Or traditional bacon - IMHO it makes a difference.', '2019-02-21 00:00:00', '2019-02-13 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Estructura de la taula `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190206081302', '2019-02-21 11:06:39'),
('20190206083421', '2019-02-21 11:06:40'),
('20190207090307', '2019-02-21 11:06:40'),
('20190208082914', '2019-02-21 11:06:40'),
('20190208085003', '2019-02-21 11:06:40'),
('20190208095051', '2019-02-21 11:06:40'),
('20190208101647', '2019-02-21 11:06:40'),
('20190211104029', '2019-02-21 11:06:40'),
('20190212114704', '2019-02-21 11:06:40'),
('20190213081632', '2019-02-21 11:06:40'),
('20190213104730', '2019-02-21 11:06:40'),
('20190214114355', '2019-02-21 11:06:40'),
('20190219093204', '2019-02-21 11:06:40'),
('20190220084155', '2019-02-21 11:06:41'),
('20190221105718', '2019-02-21 11:06:41'),
('20190221110323', '2019-02-21 11:06:41');

-- --------------------------------------------------------

--
-- Estructura de la taula `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `tag`
--

INSERT INTO `tag` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Queen was to twist.', 'queen-was-to-twist', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(2, 'I\'ve got back to.', 'ive-got-back-to', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(3, 'Why, she\'ll eat a.', 'why-shell-eat-a', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(4, 'Soup!.', 'soup', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(5, 'March Hare went.', 'march-hare-went', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(6, 'What made you so.', 'what-made-you-so', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(7, 'YOU do it!--That I.', 'you-do-it-that-i', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(8, 'Alice looked.', 'alice-looked', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(9, 'They are waiting.', 'they-are-waiting', '2019-02-12 12:52:41', '2019-02-12 12:52:41'),
(10, 'I suppose?\' said.', 'i-suppose-said', '2019-02-12 12:52:41', '2019-02-12 12:52:41');

-- --------------------------------------------------------

--
-- Estructura de la taula `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `first_name`, `password`, `twitter_username`) VALUES
(1, 'barspace0@example.com', '[]', 'Emmalee', '$2y$13$YAV6useUA91npBlw94mHY.PfBOqsV6xMsQOe177CbZrNBHfZI6UDO', NULL),
(2, 'barspace1@example.com', '[]', 'Jodie', '$2y$13$SxLkAqoNUg1iatpbRnMAbuhrlVbOBrP9P8Ee73CEc0Tic1Wzn06bq', 'emmett.hamill'),
(3, 'barspace2@example.com', '[]', 'Vernice', '$2y$13$VgHeLyOOCGuYS5Yh0cPjweJyqLk8jV4PXJmalcHdivU..Of9v9zJq', NULL),
(4, 'barspace3@example.com', '[]', 'Aliya', '$2y$13$tHz4q/vkqAD2J77TMLN4guxlileEk0p5gjoW82mEVlDJCL/BMeK9u', NULL),
(5, 'barspace4@example.com', '[]', 'Gwendolyn', '$2y$13$E8CpqiBI3o59uxQGGlw40OW2GiKshKp4M.g2Y6jritChs/ez8dCNG', 'thora.gislason'),
(6, 'barspace5@example.com', '[]', 'Ophelia', '$2y$13$p3L8uhReX/4wsG44Ex9fqOawzfhHSJwD4fTdURUnOZSfZSPa8rL9S', 'jovany71'),
(7, 'barspace6@example.com', '[]', 'Mable', '$2y$13$NHECFZLTIdRDMe8.25zcaOTcDf29nAvBq6.j2E0mjEb2x28jvA.ke', 'domenic21'),
(8, 'barspace7@example.com', '[]', 'Torrey', '$2y$13$fkCxk2x8CK2A.ItGxvgljeTB64ih/H6ipuVsVu2wzcK4G2tTtboTi', 'toy91'),
(9, 'barspace8@example.com', '[]', 'Bernadine', '$2y$13$wLxDmmFnS54HmuOTMrqfDuRYC3rnt2867VvceqqOB6vgi/jcOAopu', 'cleta.vandervort'),
(10, 'barspace9@example.com', '[]', 'Otilia', '$2y$13$GicZIBaLLuvsS2pGPEvFLuEARZxNMvnVzNC7VcGOoEx9PhVabAwoi', 'reyes.jacobs'),
(11, 'admin0@thebarspace.com', '["ROLE_ADMIN"]', 'Alverta', '$2y$13$3sdFXGIehBH8RO4jEESbXORRbBhJyjPdMql0Xkkx5zB8wzq9c7hmK', NULL),
(12, 'admin1@thebarspace.com', '["ROLE_ADMIN"]', 'Keaton', '$2y$13$eFkwysi0x.iMPaHPFTG7ru1O9ZrrruE95CFP/AUCQFb4Unlu8MpCq', NULL),
(13, 'admin2@thebarspace.com', '["ROLE_ADMIN"]', 'Madisen', '$2y$13$36UUqPRvG7nhC5JHAvF6I.7jQa19WPEP.YG1H10p5KMLSX6RLWtXK', NULL);

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7BA2F5EBA76ED395` (`user_id`);

--
-- Index de la taula `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_23A0E66989D9B62` (`slug`),
  ADD KEY `IDX_23A0E66F675F31B` (`author_id`);

--
-- Index de la taula `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`article_id`,`tag_id`),
  ADD KEY `IDX_919694F97294869C` (`article_id`),
  ADD KEY `IDX_919694F9BAD26311` (`tag_id`);

--
-- Index de la taula `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526C7294869C` (`article_id`);

--
-- Index de la taula `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index de la taula `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_389B783989D9B62` (`slug`);

--
-- Index de la taula `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `api_token`
--
ALTER TABLE `api_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT per la taula `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la taula `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT per la taula `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la taula `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Restriccions per taules bolcades
--

--
-- Restriccions per la taula `api_token`
--
ALTER TABLE `api_token`
  ADD CONSTRAINT `FK_7BA2F5EBA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Restriccions per la taula `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E66F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Restriccions per la taula `article_tag`
--
ALTER TABLE `article_tag`
  ADD CONSTRAINT `FK_919694F97294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_919694F9BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Restriccions per la taula `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
