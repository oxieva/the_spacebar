$(document).ready(function() {

    $('.js-like-article').on('click', function(e) {
        /*Pq el navegador no segueixi el link*/
        e.preventDefault();
        /*El link al q fem click*/
        var $link = $(e.currentTarget);

        $link.toggleClass('fa-heart-o').toggleClass('fa-heart');

        $.ajax({
            method: 'POST',
            url: $link.attr('href')
        }).done(function(data) {
            $('.js-like-article-count').html(data.hearts);
        })
    });
});